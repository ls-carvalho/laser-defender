﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public static int scoreField = 0;

	private Text textArea;

	void Start(){
		textArea = GetComponent<Text>();
		Reset();
	}

	void Update(){
		textArea.text = "Score: "+scoreField;
	}

	public void Score(int points){
		scoreField += points;
	}

	public static void Reset(){
		scoreField = 0;
	}
}
