﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreDisplay : MonoBehaviour {
	Text endScore;

	void Start(){
		endScore = GetComponent<Text>();
		endScore.text = ScoreKeeper.scoreField.ToString();
	}

}
