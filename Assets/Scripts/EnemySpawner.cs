﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject[] enemyPrefab;
	public float width = 10f;
	public float height = 5f;
	public float velocity = 3f;
	public float spawnDelay = 0.5f;

	private float xMin;
	private float xMax;
	private bool direction;
	
	void Start () {
		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distanceToCamera));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distanceToCamera));
		xMin = leftmost.x+(width/2);
		xMax = rightmost.x-(width/2);
		SpawnUntilFull();
	}

	void Update () {
		if(direction){
			transform.position += Vector3.left * velocity * Time.deltaTime;
		}else{
			transform.position += Vector3.right * velocity * Time.deltaTime;
		}
		if(transform.position.x <= xMin){
			direction=false;
		}else if(transform.position.x >=xMax){
			direction=true;
		}
		if(AllMembersDead()){
			SpawnUntilFull();
		}
	}

	public void OnDrawGizmos(){
		Gizmos.DrawWireCube(transform.position,new Vector3(width,height));
	}

	Transform NextFreePosition(){
		foreach(Transform child in transform){
			if(child.childCount==0){
				return child;
			}
		}
		return null;
	}

	bool AllMembersDead(){
		foreach(Transform child in transform){
			if(child.childCount>0){
				return false;
			}
		}
		return true;
	}

	void SpawnUntilFull(){
		Transform freePosition = NextFreePosition();
		if(freePosition){
			GameObject enemy = Instantiate(enemyPrefab[0],freePosition.position,Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
			if(Random.Range(1,5)<=2){
				direction = false;
			}else{
				direction = true;
			}
		}
		if(NextFreePosition()){
			Invoke("SpawnUntilFull", spawnDelay);
		}
	}

	void EnemySpawn(){
		foreach(Transform child in transform){
			GameObject enemy = Instantiate(enemyPrefab[0],child.transform.position,Quaternion.identity) as GameObject;
			enemy.transform.parent = child;
		}
		if(Random.Range(1,5)<=2){
			direction = false;
		}else{
			direction = true;
		}
	}
}
