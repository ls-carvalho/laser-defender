﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public GameObject laserType;
	public float velocity=5f;
	public float playerAdjust=0.5f;
	public float laserSpeed = 4f;
	public float firingRate = 0.2f;
	public float health = 250f;

	private float xMin;
	private float xMax;
	private LevelManager lm;

	void Start () {
		lm = GameObject.FindObjectOfType<LevelManager>();
		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distanceToCamera));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distanceToCamera));
		xMin = leftmost.x+playerAdjust;
		xMax = rightmost.x-playerAdjust;
	}

	void Update () {
		MovingShip();
	}

	void Fire(){
		GameObject pew = Instantiate(laserType,this.transform.position,Quaternion.identity) as GameObject;
		pew.rigidbody2D.velocity = new Vector2(0,laserSpeed);
	}

	void MovingShip(){
		if(Input.GetKey(KeyCode.LeftArrow)){
			this.transform.position += Vector3.left * velocity * Time.deltaTime;
		}
		if(Input.GetKey(KeyCode.RightArrow)){
			this.transform.position += Vector3.right * velocity * Time.deltaTime;
		}
		if(Input.GetKeyDown(KeyCode.Space)){
			InvokeRepeating("Fire", 0.000001f, firingRate);	//Small time increment to avoid bug (multiples projectiles)
		}
		if(Input.GetKeyUp(KeyCode.Space)){
			CancelInvoke("Fire");
		}
		float newX = Mathf.Clamp(transform.position.x,xMin,xMax);
		this.transform.position = new Vector3(newX,this.transform.position.y,this.transform.position.z);
	}

	void OnTriggerEnter2D(Collider2D collider){
		LaserRed missile = collider.gameObject.GetComponent<LaserRed>();
		if(missile){
			health -= missile.GetDamage();
			missile.Hit();
			if(health<=0){
				Destroy(gameObject);
				lm.LoadNextLevel();
			}
		}
	}
}
