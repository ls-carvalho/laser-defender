﻿using UnityEngine;
using System.Collections;

public class LaserRed : MonoBehaviour {
	
	public float damage = 100f;
	
	void OnBecameInvisible() {
		Destroy(gameObject);
	}
	
	public float GetDamage(){
		return damage;
	}
	
	public void Hit(){
		Destroy(gameObject);
	}
}
