﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {

	public float damage = 100f;

	void OnBecameInvisible() { //Used this instead of creating a "Shredder"
		Destroy(gameObject);
	}

	public float GetDamage(){
		return damage;
	}

	public void Hit(){
		Destroy(gameObject);
	}
}
