﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public GameObject laserType;
	public float health = 150f;
	public float laserSpeed = -4f;
	public float firingRate = 3f;
	public float startingFire = 1.5f;
	public bool randomFire = false;
	public int scoreValue = 10;
	public AudioClip deathSoundEffect;
	public float deathSoundVolume = 1f;

	private ScoreKeeper sk;
	private float timedFire;
	private bool isFiring;

	void Start(){
		sk = GameObject.Find("Score").GetComponent<ScoreKeeper>();
		isFiring=false;
		timedFire=0;
	}

	void Update(){
		if(randomFire){
			RandomicEnemyFire();
		}else{
			EnemyFire();
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		Laser missile = collider.gameObject.GetComponent<Laser>();
		if(missile){
			health -= missile.GetDamage();
			missile.Hit();
			if(health<=0){
				sk.Score(scoreValue);
				AudioSource.PlayClipAtPoint(deathSoundEffect,transform.position,deathSoundVolume);
				Destroy(gameObject);
			}
		}
	}

	void EnemyFire(){
		if(isFiring){
			timedFire += Time.deltaTime;
			if(timedFire>=firingRate&&firingRate>0){
				GameObject pew = Instantiate(laserType,this.transform.position,Quaternion.identity) as GameObject;
				pew.rigidbody2D.velocity = new Vector2(0,laserSpeed);
				timedFire=0;
			}
		}else{
			timedFire+=Time.deltaTime;
			if(timedFire>=startingFire){
				isFiring=true;
			}
		}
	}

	void RandomicEnemyFire(){
		float prob = Time.deltaTime * firingRate;
		if(Random.value < prob){
			GameObject pew = Instantiate(laserType,this.transform.position,Quaternion.identity) as GameObject;
			pew.rigidbody2D.velocity = new Vector2(0,laserSpeed);
		}
	}
}
